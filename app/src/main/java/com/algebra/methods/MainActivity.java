package com.algebra.methods;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUsername;
    private EditText etPassword;
    private Button bLogin;

    private static final String PASSWORD = "123abc";
    private static final String USERNAME = "Algebra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void initWidgets() {
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        bLogin = findViewById(R.id.bLogin);
    }

    private void setupListeners() {
        bLogin.setOnClickListener(view -> {

            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();

            boolean usernameOk = isUsernameOk(username);
            boolean passwordOk = isPasswordOk(password);

            boolean isLoginSuccess = usernameOk && passwordOk;

            String loginMessage = getLoginMessage(isLoginSuccess);

            //Toast.makeText(this, loginMessage, Toast.LENGTH_LONG).show();

            printMessage(loginMessage);
            clearInputs(isLoginSuccess);
        });
    }

    private void clearInputs(boolean isLoginSuccess) {
        if (isLoginSuccess) {
            etUsername.setText("");
            etPassword.setText("");
            etUsername.requestFocus();
        }
    }

    private void printMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private boolean isUsernameOk(String username) {
        if (username.isEmpty()) {
            etUsername.setError("Username is empty");
            return false;
        }

        if (!username.equals(USERNAME)) {
            etUsername.setError("Username not valid");
            return false;
        }

        return true;
    }

    private boolean isPasswordOk(String password) {
        if (password.isEmpty()) {
            etPassword.setError("Password is empty");
            return false;
        }

        if (!password.equals(PASSWORD)) {
            etPassword.setError("Password not valid");
            return false;
        }

        return true;
    }

    private String getLoginMessage(boolean isSuccess) {
        if (isSuccess) {
            return "Login successful";
        }
        return "Login failed";
    }
}
